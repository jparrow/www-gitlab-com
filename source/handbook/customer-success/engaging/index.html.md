---
layout: markdown_page
title: "Engaging a Customer Success Engineer"
---

# When to engage a Success Engineer
- Customer/prospect has been identified as
   - Key customer
   - Has or is interested in more than 100 licenses
- Customer has specific technical challenges with GitLab and could use an internal advocate to manage them
   - Feature requests
   - Performance / configuration questions
   - Other

# How to engage a Success Engineer
- Identify a Success Engineer in the appropriate time zone / region
- Discuss customer/prospect with Success Engineer via email/hangout/Slack/etc.
- If desirable, schedule a call with the customer/prospect and Success Engineer

