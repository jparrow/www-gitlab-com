---
layout: markdown_page
title: "Design"
---
# Welcome to the Design Handbook

[Up one level to the Marketing Handbook](/handbook/marketing/)    

## On this page
* [Requesting design help](#designHelp)
* [`Design`label in issue tracker](#designLabel)
* [Project prioritization](#priority)
* [Design touchpoints](#touchpoints)
* [Design onboarding](#onboarding)
* [Design Sync](#designSync)
* [Design review process](#review)
* [Handing design off to front-end](#front-end)
* [Team GitLab Dribbble account](#dribbble)
* [Slack design channels](#chat)  
* [Design tools](#designTools)
* [Inspiration, who does design well?](#inspiration)

## Design Handbooks:  

- [Design](/handbook/marketing/design/)  
- [Brand Guidelines](/handbook/marketing/design/brand-guidelines/)   

## Requesting design help<a name="designHelp"></a>

1. Create an issue in the corresponding project repository
  1. For tasks pertaining to [about.gitlab.com](about.gitlab.com) create an issue in the [www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com/issues).
  1. For all other marketing related tasks create an issue in the [marketing project](https://gitlab.com/gitlab-com/marketing/issues).
1. Add all relevant details, goal(s), purpose, resources, and links in the issue description. Also `@`mention team members who will be involved.
1. Set due date (if possible).
1. Label issues with `Design`.
